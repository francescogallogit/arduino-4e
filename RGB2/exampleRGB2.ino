/*
Lo sketch consente all'utente di inserire mediante
l'input da serial tre valori
utili al diodo RGB per visualizzare un colore specifico.
Es: r=255, b=255. g=255.. Qual e' il colore?
*/

int red = 11;  // porta 11 - anodo rosso
int blu = 6;   // porta 6  - anodo blu
int green = 5; // porta 5  - anodo verde

int r = 0;
int b = 0;
int g = 0;

void color(int re, int gr, int bl) {
  
  analogWrite(red, re);
  analogWrite(blu, bl);
  analogWrite(green, gr);
  
}


void setup() {
  Serial.begin(9600);      // Inizializza Porta Seriale
  
  pinMode(red, OUTPUT);
  pinMode(blu, OUTPUT);
  pinMode(green, OUTPUT);
  
  Serial.println("<Arduino Pronto>");
}
 
void loop() {
  
  Serial.print("Inserire il valore di r: ");
  while (Serial.available()==0){}             
  r = Serial.parseInt();
  Serial.println(r);
 
  
  Serial.print("Inserire il valore di b: ");
  while (Serial.available()==0){}             
  b = Serial.parseInt(); 
  Serial.println(b);
 
  
  Serial.print("Inserire il valore di g: ");
  while (Serial.available()==0){}             
  g = Serial.parseInt();
  Serial.println(g);
 
  
  color(r, b, g);
  delay(5000);
  Serial.println("-----------------");
  
}
  