/*** DEFINIZIONE PIN  ***/
#define PIN_A		5
#define PIN_B		6
#define PIN_AND		8
#define PIN_OR		9
#define PIN_NAND	10
#define PIN_NOR		11
#define PIN_EXOR	12
#define PIN_EXNOR	13


/*** Variabili Stato ***/
bool andState = false;
bool orState = false;
bool nandState = false;
bool norState = false;
bool exorState = false;
bool exnorState = false;

/*** Variabili input ***/
bool inA, inB;
int outPin[6] = {PIN_AND, PIN_OR, PIN_NAND, PIN_NOR, PIN_EXOR, PIN_EXNOR};

void setup()
{
  pinMode(PIN_A, INPUT);
  pinMode(PIN_B, INPUT);
  
  //Ingressi e Uscite settati a LOW
  for(int i = 0; i < 6; i++)
  {
    pinMode(outPin[i], OUTPUT);
    digitalWrite(outPin[i], LOW);
  }
  
}

void loop()
{
  readInput(&inA, &inB);//lettura degli ingressi
  andState = logicAnd(inA, inB);//funzione And
  orState = logicOr(inA, inB);//funzione Or
  nandState = !logicAnd(inA, inB);//funzione Nand
  norState = !logicOr(inA, inB);//funzione Nor
  exorState = logicExor(inA, inB);//funzione Exor
  exnorState = !logicExor(inA, inB);//funzione Exnor  
  setOut();//setta uscite
  delay(5);//ritardo sistema  
}

/*** Lettura Ingressi ***/
void readInput(bool *a, bool *b)
{
  *a = digitalRead(PIN_A);
  *b = digitalRead(PIN_B);
}

/*** Pilotaggio LED ***/
void setOut()
{
  digitalWrite(PIN_AND, andState);
  digitalWrite(PIN_OR, orState);
  digitalWrite(PIN_NAND, nandState);
  digitalWrite(PIN_NOR, norState);
  digitalWrite(PIN_EXOR, exorState);
  digitalWrite(PIN_EXNOR, exnorState);
}


bool logicAnd(bool a, bool b)
{
  if(a && b)
    return true;
  else
    return false;  
}


bool logicOr(bool a, bool b)
{
  if(a || b)
    return true;
  else
    return false;  
}


bool logicExor(bool a, bool b)
{
  if(a != b)
    return true;
  else
    return false;  
}