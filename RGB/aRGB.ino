int red = 11;  // porta 11 - anodo rosso
int blu = 6;   // porta 6  - anodo blu
int green = 5; // porta 5  - anodo verde

void color(int r, int b, int g) {
  analogWrite(red, r);
  analogWrite(blu, b);
  analogWrite(green, g);
}
void setup() {
  pinMode(red, OUTPUT);
  pinMode(blu, OUTPUT);
  pinMode(green, OUTPUT);
  randomSeed(analogRead(A0));
}
void loop() {
  int r = random(0, 256);
  color(r, 0, 0);
  delay(3000);
  
  int b = random(0, 256);
  color(0, b, 0);
  delay(3000);
    
  int g = random(0, 256);
  color(0, 0, g);
  delay(3000);
}
