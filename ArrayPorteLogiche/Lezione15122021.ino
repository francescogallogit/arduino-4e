/* Test Integrato AND Logico con array */

const int size = 10;

int in1 = 2;
int in2 = 4;

int i1[size];
int i2[size];

void createInput() {
  
  int a, b;
  
  for (int i = 0; i < size; i++) {
    
    a = random(0, 2);
    i1[i] = a;
    
    b = random(0, 2);
    i2[i] = b;
    
    digitalWrite(2, i1[i]);
    digitalWrite(4, i1[i]);   
    
    Serial.print(i1[i]);
    Serial.print(" ");
    Serial.print(i2[i]); 
    
    
    delay(3000);
    Serial.print("\n");
    
  }
  
  
}


void setup()
{
  Serial.begin(9600);
  
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  Serial.println("Arduino Ready...");
  
}

void loop()
{
  
  createInput();
  Serial.println("----------");
  delay(3000);
  
}