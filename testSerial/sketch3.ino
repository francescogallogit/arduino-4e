void setup() {
  Serial.begin(9600);
  Serial.write("Digitare un carattere: \n");
}

char valore = 0;

void loop() {
  if (Serial.available() > 0) { //In attesa di un carattere da tastiera
    valore = Serial.read();     // legge il carattere
    // se viene ricevuto un valore numerico
    if ((valore >= '0') && (valore <= '9')) {
      Serial.print("Numero ricevuto: ");
      Serial.println(valore);
    }
    else {
      Serial.println("Non e' un numero.");
    }
  } // end if
}