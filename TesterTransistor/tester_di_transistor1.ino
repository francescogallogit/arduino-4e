int term1;
int term2;
int term3;

int gtr50;
int less5;

int npn;
int pnp;

int col;
int npnbas;
int emi;
int pnpbas;

void setup() {
	pinMode(7, OUTPUT);
	pinMode(8, OUTPUT);
	pinMode(12, OUTPUT);
	Serial.begin(9600);
}

void loop() {
	digitalWrite(7, LOW);
	digitalWrite(8, LOW);
	digitalWrite(12, LOW);
	gtr50 = 0;
	less5 = 0;

//esamina la prima combinazione settando a High il primo pin
digitalWrite(12, HIGH);
term1 = analogRead(A1);
term2 = analogRead(A2);
term3 = analogRead(A3);

if (term1 == 0) {less5 = less5 + 1;}
if (term2 == 0) {less5 = less5 + 1;}
if (term3 == 0) {less5 = less5 + 1;}
if (less5 == 2) {pnpbas = 1;}

if (term1 > 50) { gtr50=gtr50+1; }
if (term2 > 50) { gtr50=gtr50+1; }
if (term3 > 50) { gtr50=gtr50+1; }
  
if (term1>50 && term2>50 && term3>50) {
  npnbas=1;
}
Serial.print(term1);
Serial.print(" ");
Serial.print(term2);
Serial.print(" ");
Serial.print(term3);
Serial.print(" ");
Serial.println(less5);
less5=0;
delay(2000);

//esamina la seconda combinazione settando a High il secondo pin
digitalWrite(12, LOW);
digitalWrite(8, HIGH);
term1 = analogRead(A1);
term2 = analogRead(A2);
term3 = analogRead(A3);
if (term1==0) {less5 = less5 + 1;}
if (term2==0) {less5 = less5 + 1;}
if (term3==0) {less5 = less5 + 1;}
if (less5==2) {pnpbas = 2;}
  
if (term1 > 50) { gtr50=gtr50+1; }
if (term2 > 50) { gtr50=gtr50+1; }
if (term3 > 50) { gtr50=gtr50+1; }
if (term1>50 && term2>50 && term3>50) {npnbas=2;}
  
Serial.print(term1);
Serial.print(" ");
Serial.print(term2);
Serial.print(" ");
Serial.print(term3);
Serial.print(" ");
Serial.println(less5);
less5=0;
delay(2000);

//esamina la terza combinazione settando a High il terzo pin
digitalWrite(8, LOW);
digitalWrite(7, HIGH);
term1 = analogRead(A1);
term2 = analogRead(A2);
term3 = analogRead(A3);
if (term1==0) {less5 = less5 + 1;}
if (term2==0) {less5 = less5 + 1;}
if (term3==0) {less5 = less5 + 1;}
if (less5==2) {pnpbas = 3;}
if (term1 > 50) { gtr50=gtr50+1; }
if (term2 > 50) { gtr50=gtr50+1; }
if (term3 > 50) { gtr50=gtr50+1; }
  
if (term1>50 && term2>50 && term3>50) {npnbas=3;}
Serial.print(term1);
Serial.print(" ");
Serial.print(term2);
Serial.print(" ");
Serial.print(term3);
Serial.print(" ");
Serial.print(less5);
Serial.print(" ");
less5=0;

if (gtr50==5) {
	npn=1;
	Serial.print("transistor NPN");
	Serial.print(" - base su terminale ");
	Serial.print(npnbas); }
else
	if (gtr50==7) {
		pnp=1;
		Serial.print("transistor PNP");
		Serial.print(" - base su terminale ");
		Serial.print(pnpbas);
	}

Serial.println();
Serial.println();
delay(2000);

}