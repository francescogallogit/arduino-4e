int i = 0; //Contatore

void setup() {
  
  Serial.begin(9600);

  pinMode(12, OUTPUT);

}

void loop() {
  
  if(Serial.available()) {
    
    char val = Serial.read();

    if(val == '1') {
      digitalWrite(12, HIGH);
    } else {
      digitalWrite(12, LOW);
    }
  }

}
