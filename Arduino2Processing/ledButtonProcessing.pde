import processing.serial.*;

Serial seriale;
boolean ledOn = false;

int counter = 0;
PFont font; //https://processing.org/reference/PFont.html

void setup() {
  
  /*
  for(int i = 0; i < Serial.list().length; i++) {
    print(i + " ");
    println(Serial.list()[i]);
  }*/
  
  size(500, 300); //https://processing.org/reference/size_.html
  String porta = Serial.list()[2];
  seriale = new Serial(this, porta, 9600);
  
  font = createFont("Arial", 100); //https://processing.org/reference/createFont_.html
  
}

void draw() {
  background(190); //https://processing.org/reference/background_.html
  
  if(mousePressed && !ledOn) {
    seriale.write("on\n");
    ledOn = true;
    
    counter++;
  } else if(mousePressed && ledOn){
    seriale.write("off\n");
    ledOn = false;
  }
  
  if(ledOn) {
    fill(230, 0, 0, 200);//https://processing.org/reference/fill_.html
  } else {
    fill(255, 0, 0, 80);
  }
  // Parte Grafica
  stroke(50); //https://processing.org/reference/stroke_.html
  arc(150, 80, 70, 70, -PI, 0); //https://processing.org/reference/arc_.html
  line(115, 80, 115, 130); //https://processing.org/reference/line_.html
  line(185, 80, 185, 130);
  noStroke(); //https://processing.org/reference/noStroke_.html
  rect(115, 80, 70, 50); //https://processing.org/reference/rect_.html
  stroke(50);
  rect(100, 130, 100, 20); 
  fill(127);
  rect(130, 150, 10, 100);
  rect(160, 150, 10, 80);
  
  if(seriale.available() > 0) {
    String comando = seriale.readStringUntil('\n');
    if(comando != null && trim(comando).equals("on")) {
      counter = 0;
    }
  }
  fill(0); //https://processing.org/reference/fill_.html
  textFont(font); //https://processing.org/reference/textFont_.html
  textAlign(CENTER); //https://processing.org/reference/textAlign_.html
  text(counter, 350, 180); //https://processing.org/reference/text_.html
  
}
