#define led 12
#define button 2

int LButtonState = LOW;

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);

  pinMode(button, INPUT);

}

void loop() {

  int buttonState = digitalRead(button);
  if(buttonState != LButtonState && buttonState == HIGH) {
    Serial.println("on");
  }
  LButtonState = buttonState;
  
  if(Serial.available()) {
    String comando = Serial.readStringUntil('\n');
    digitalWrite(led, comando == "on");
  }
  delay(50);

}
